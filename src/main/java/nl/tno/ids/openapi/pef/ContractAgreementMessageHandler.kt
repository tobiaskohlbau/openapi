package nl.tno.ids.openapi.pef

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.infomodel.isResponseTo
import nl.tno.ids.base.infomodel.toMultiPartMessage
import nl.tno.ids.common.serialization.SerializationHelper
import nl.tno.ids.openapi.PolicyManager
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component

@Component
class ContractAgreementMessageHandler(
  private val policyNegotiation: PolicyNegotiation,
  @Nullable private val policyManager: PolicyManager?
): StringMessageHandler<ContractAgreementMessage> {
  companion object {
    private val LOG = LoggerFactory.getLogger(ContractAgreementMessageHandler::class.java)
  }
  override fun handle(header: ContractAgreementMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    LOG.info("Received Contract Agreement Message")
    return policyNegotiation.offersInflight[header.correlationMessage]?.let { offer ->
      val agreement = SerializationHelper.getInstance().fromJsonLD(payload, ContractAgreement::class.java)
      if (offer.permission.size == agreement.permission.size &&
        offer.prohibition.size == agreement.prohibition.size &&
        offer.obligation.size == agreement.obligation.size &&
        offer.permission.all { a -> agreement.permission.any { b -> comparePermission(a, b) } } &&
        offer.prohibition.all { a -> agreement.prohibition.any { b -> compareRule(a, b) } } &&
        offer.obligation.all { a -> agreement.obligation.any { b -> compareRule(a, b) } }) {
        policyManager?.addContractAgreement(agreement)
        LOG.info("Contract agreement ${agreement.id} accepted")
        val contractAgreementMessage = ContractAgreementMessageBuilder()
          .build()
          .isResponseTo(header)
          .toMultiPartMessage(
            payload = payload,
            contentType = "application/ld+json"
          )
        ResponseEntity.ok(contractAgreementMessage)
      } else {
        LOG.warn("Contract agreement does not match contract offer")
        val contractRejectionMessage = ContractRejectionMessageBuilder()
          ._contractRejectionReason_(TypedLiteral("Contract agreement does not match contract offer", "en"))
          ._rejectionReason_(RejectionReason.NOT_FOUND)
          .build()
          .isResponseTo(header)
          .toMultiPartMessage(
            payload = """
              {
                "error": "ContractAgreementMismatch",
                "message": "Contract agreement does not match contract offer"
              }
            """.trimIndent()
          )
        ResponseEntity.status(HttpStatus.NOT_FOUND).body(contractRejectionMessage)
      }
    } ?: run {
      LOG.warn("No corresponding contract offer found")
      val contractRejectionMessage = ContractRejectionMessageBuilder()
        ._contractRejectionReason_(TypedLiteral("No corresponding contract offer found", "en"))
        ._rejectionReason_(RejectionReason.NOT_FOUND)
        .build()
        .isResponseTo(header)
        .toMultiPartMessage(
          payload = """
              {
                "error": "ContractOfferNotFound",
                "message": "No corresponding contract offer found"
              }
            """.trimIndent()
        )
      ResponseEntity.status(HttpStatus.NOT_FOUND).body(contractRejectionMessage)
    }
  }

  /**
   * Compare two Permissions, is a bit more complex than compareRule due to the possible pre/post
   * duties
   *
   * The comparison also matches when the external permission is more specific than the internal
   * permission e.g. an internal permission does not contain an assignee but the external
   * permission does
   */
  private fun comparePermission(internal: Permission, external: Permission): Boolean {
    return internal.assignee.size == 0 ||
        internal.assignee == external.assignee && internal.assigner.size == 0 ||
        internal.assigner == external.assigner &&
        internal.target?.equals(external.target) ?: true &&
        internal.constraint.size == 0 ||
        internal.constraint == external.constraint && internal.action.size == 0 ||
        internal.action == external.action &&
        internal.assetRefinement?.equals(external.assetRefinement) ?: true &&
        internal.preDuty.size == 0 ||
        internal.preDuty.all { a -> external.preDuty.any { b -> compareRule(a, b) } } &&
        internal.postDuty.size == 0 ||
        internal.postDuty.all { a -> external.postDuty.any { b -> compareRule(a, b) } }
  }

  /**
   * Compare two Rules
   *
   * The comparison also matches when the external permission is more specific than the internal
   * permission e.g. an internal permission does not contain an assignee but the external
   * permission does
   */
  private fun compareRule(internal: Rule, external: Rule): Boolean {
    return internal.assignee.size == 0 ||
        internal.assignee == external.assignee && internal.assigner.size == 0 ||
        internal.assigner == external.assignee &&
        internal.target?.equals(external.target) ?: true &&
        internal.constraint.size == 0 ||
        internal.constraint == external.constraint && internal.action.size == 0 ||
        internal.action == external.action &&
        internal.assetRefinement?.equals(external.assetRefinement) ?: true
  }
}