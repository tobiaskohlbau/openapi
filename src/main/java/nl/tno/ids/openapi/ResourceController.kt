package nl.tno.ids.openapi

import javax.servlet.http.HttpServletRequest
import org.springframework.core.io.FileSystemResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod


@RequestMapping
class ResourceController {
  @RequestMapping(value = ["/"], method = [RequestMethod.GET])
  fun index(): ResponseEntity<*> {
    return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(FileSystemResource("/static/index.html"))
  }

  @RequestMapping(
    value = ["/**/{file:.*\\.(?:html|js|css|jpeg|png|ico)$}"],
    method = [RequestMethod.GET, RequestMethod.POST]
  )
  fun staticResources(request: HttpServletRequest): ResponseEntity<*> {
    val fileSystemResource = FileSystemResource("/static/${request.requestURI}")
    return ResponseEntity.ok().contentType(staticMediaType(request.requestURI)).body(fileSystemResource)
  }

  private fun staticMediaType(filename: String): MediaType {
    return when (filename.substringAfterLast('.')) {
      "html" -> MediaType.TEXT_HTML
      "js" -> MediaType.parseMediaType("text/javascript")
      "css" -> MediaType.parseMediaType("text/css")
      "map" -> MediaType.APPLICATION_JSON
      else -> MediaType.TEXT_PLAIN
    }
  }
}