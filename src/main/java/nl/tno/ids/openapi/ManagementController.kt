package nl.tno.ids.openapi

import nl.tno.ids.base.BrokerClient
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping(value = ["/mgmt"])
class ManagementController(
    private val agentsController: AgentsController,
    private val brokerClient: BrokerClient,
    private val openApiConfig: OpenApiConfig
) {
    @get:RequestMapping(
        value = ["/agents"],
        method = [RequestMethod.GET],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    val agents: CompletableFuture<Set<AgentConfig>>
        get() = CompletableFuture.supplyAsync {
            agentsController.agents
        }

    @RequestMapping(value = ["/config"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getConfig(): OpenApiConfig {
        return openApiConfig.copy(
            agents = emptyList(),
            mongoDbConfig = null
        )
    }

    @RequestMapping(value = ["/agents"], method = [RequestMethod.POST], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun addAgent(@RequestBody agentConfig: AgentConfig): CompletableFuture<ResponseEntity<Any?>>? {
        return CompletableFuture.supplyAsync {
            agentsController.addAgent(agentConfig)
            ResponseEntity.status(201).build()
        }
    }

    @RequestMapping(value = ["/agents/{id}"], method = [RequestMethod.PUT], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun modifyAgent(
        @RequestBody agentConfig: AgentConfig,
        @PathVariable id: String
    ): CompletableFuture<ResponseEntity<Any?>>? {
        return CompletableFuture.supplyAsync {
            agentsController.modifyAgent(id, agentConfig)
            ResponseEntity.status(200).build()
        }
    }

    @RequestMapping(value = ["/agents/{id}"], method = [RequestMethod.DELETE])
    fun deleteAgent(@PathVariable id: String): CompletableFuture<ResponseEntity<Any?>>? {
        return CompletableFuture.supplyAsync {
            agentsController.removeAgent(id)
            ResponseEntity.status(200).build()
        }
    }

    @RequestMapping(value = ["/cache"], method = [RequestMethod.DELETE])
    fun invalidateCache(): CompletableFuture<ResponseEntity<Any?>>? {
        return CompletableFuture.supplyAsync {
            brokerClient.invalidateCaches()
            ResponseEntity.status(200).build()
        }
    }
}