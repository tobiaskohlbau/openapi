/*
 * Copyright 2016 Fraunhofer-Institut für Materialfluss und Logistik IML und Fraunhofer-Institut für Software- und Systemtechnik ISST
 * Industrial Data Space, Reference Use Case Logistics
 *
 * For license information or if you have any questions contact us at ids-ap2@isst.fraunhofer.de.
 */
package nl.tno.ids.openapi

import org.apache.catalina.connector.Connector
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.tomcat.util.buf.EncodedSolidusHandling
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.util.UrlPathHelper


/**
 * This is a Spring Boot Application.
 *
 * @author [Maarten Kollenstart](mailto:maarten.kollenstart@tno.nl)
 * @see [Spring Boot](http://projects.spring.io/spring-boot/)
 */
@SpringBootApplication(
    exclude = [DataSourceAutoConfiguration::class, DataSourceTransactionManagerAutoConfiguration::class, HibernateJpaAutoConfiguration::class, AopAutoConfiguration::class, MongoAutoConfiguration::class, MongoDataAutoConfiguration::class],

    scanBasePackages = ["nl.tno.ids"]
)
@ConfigurationPropertiesScan("nl.tno.ids")
class Application: WebMvcConfigurer {

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        val urlPathHelper = UrlPathHelper()
        urlPathHelper.isUrlDecode = false
        configurer.setUrlPathHelper(urlPathHelper)
    }

    @Bean
    fun tomcatCustomizer(): WebServerFactoryCustomizer<TomcatServletWebServerFactory>? {
        return WebServerFactoryCustomizer { factory: TomcatServletWebServerFactory ->
            factory.addConnectorCustomizers(
                TomcatConnectorCustomizer { connector: Connector ->
                    connector.encodedSolidusHandling = EncodedSolidusHandling.PASS_THROUGH.value
                })
        }
    }
}

@Configuration
class StaticResourceConfiguration : WebMvcConfigurer {
    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/**").addResourceLocations("file:/static/")
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
