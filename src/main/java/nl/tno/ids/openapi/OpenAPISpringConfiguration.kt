package nl.tno.ids.openapi

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class OpenAPISpringConfiguration {
    @Bean
    @Primary
    fun openApiHttpClient(): CloseableHttpClient {
        val timeout = 180
        val config = RequestConfig.custom()
            .setConnectTimeout(timeout * 1000)
            .setConnectionRequestTimeout(timeout * 1000)
            .setSocketTimeout(timeout * 1000).build()
        return HttpClientBuilder.create().setDefaultRequestConfig(config).build()
    }

    /** Jackson Object mapper including the Kotlin Module for Kotlin Data Class support */
    @Bean
    @Primary
    fun objectMapperOpenAPI(): ObjectMapper {
        return JsonMapper.builder()
            .addModule(KotlinModule.Builder().build())
            .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
            .build()
    }
}