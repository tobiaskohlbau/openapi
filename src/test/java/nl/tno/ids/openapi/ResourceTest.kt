package nl.tno.ids.openapi

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.mongo.transitions.Mongod
import de.flapdoodle.embed.mongo.transitions.RunningMongodProcess
import de.flapdoodle.reverse.TransitionWalker
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.Conventions
import org.bson.codecs.pojo.PojoCodecProvider
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["spring.config.location=classpath:application.yaml"])
@EnableAutoConfiguration(exclude = [MongoAutoConfiguration::class, MongoDataAutoConfiguration::class])
class ResourceTest {

    @LocalServerPort
    private lateinit var port: String

    @Autowired
    private lateinit var agentsController: AgentsController

    @Test
    fun testHealthCheck() {
        HttpClients.createDefault().execute(HttpGet("http://localhost:$port/health")).use {
            Assertions.assertEquals(200, it.statusLine.statusCode)
        }
    }

    @Test
    fun testMongoInteraction() {
        Assertions.assertEquals(3, agentsController.agents.size)
        collection.insertOne(
            AgentConfig(
                id = "urn:scsn:34567890",
                title = "TNO SCSN Test Agent 3",
                config = mapOf(
                    "connector" to mapOf(
                        "SUPPORTEDMESSAGES" to "Order 2.0;OrderResponse 2.0"
                    )
                ),
                backendUrl = "http://localhost:0"
            )
        )
        Thread.sleep(2500)
        Assertions.assertEquals(4, agentsController.agents.size)
        collection.deleteOne(Filters.eq("id", "urn:scsn:12345678"))
        Thread.sleep(5000)
        Assertions.assertEquals(3, agentsController.agents.size)
        wiremock.verify(3, postRequestedFor(anyUrl()))
        wiremock.verify(1, deleteRequestedFor(anyUrl()))
    }

    companion object {
      private lateinit var mongod: TransitionWalker.ReachedState<RunningMongodProcess>
      private lateinit var mongoClient: MongoClient
      private lateinit var wiremock: WireMockServer
        private lateinit var collection: MongoCollection<AgentConfig>

        @BeforeAll
        @JvmStatic
        fun setupMongo() {
          mongod = Mongod.instance().start(Version.Main.V6_0)

          System.setProperty("openApi.mongoDbConfig.port", "${mongod.current().serverAddress.port}")
          mongoClient = MongoClients.create(
            MongoClientSettings.builder()
              .applyConnectionString(ConnectionString("mongodb://${mongod.current().serverAddress}"))
              .codecRegistry(
                CodecRegistries.fromRegistries(
                    MongoClientSettings.getDefaultCodecRegistry(),
                    CodecRegistries.fromProviders(
                        PojoCodecProvider.builder()
                            .conventions(listOf(Conventions.ANNOTATION_CONVENTION, Conventions.USE_GETTERS_FOR_SETTERS))
                            .automatic(true)
                            .build()
                    )
                )
              )
              .build()
          )
          collection = mongoClient
                .getDatabase("test")
                .getCollection("test", AgentConfig::class.java)
        }

        @BeforeAll
        @JvmStatic
        fun setupWireMock() {
            wiremock = WireMockServer(47375).apply { start() }
            wiremock.stubFor(
                any(anyUrl()).willReturn(ok())
            )
        }

        @AfterAll
        @JvmStatic
        fun destroyMongo() {
            mongoClient.close()
            mongod.close()
            System.clearProperty("openApi.mongoDbConfig.port")
        }

        @AfterAll
        @JvmStatic
        fun destroyWireMock() {
            wiremock.stop()
        }
    }
}