package nl.tno.ids.openapi

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.ResultMessageBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.base.infomodel.toMultiPartMessage
import nl.tno.ids.openapi.IDSHttpHeaders.API_METHOD
import nl.tno.ids.openapi.IDSHttpHeaders.API_PATH
import nl.tno.ids.openapi.IDSHttpHeaders.BACKEND_MASKED
import nl.tno.ids.openapi.IDSHttpHeaders.HEADER_PREFIX
import nl.tno.ids.openapi.IDSHttpHeaders.STATUS_CODE
import nl.tno.ids.openapi.IDSHttpHeaders.TYPE
import nl.tno.ids.openapi.server.OpenAPIMessageHandler
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import java.net.URI


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["spring.config.location=classpath:application-backendrouting.yaml"])
@EnableAutoConfiguration(exclude = [MongoAutoConfiguration::class, MongoDataAutoConfiguration::class])
class BackendRoutingTest {

    @LocalServerPort
    private lateinit var port: String

    @Autowired
    private lateinit var openApiMessageHandler: OpenAPIMessageHandler

    @Test
    fun testBackendRoutingWithAuth() {
        executeHandler("urn:aas:1234", "/v1/shells/www.tata.com.1687591.01.01/aas/submodels/TechnicalData/submodel/submodelElements/FurtherInformation/Test")
        wiremock.verify(exactly(1), getRequestedFor(urlMatching("/aas1234/shells/www.tata.com.1687591.01.01/aas/submodels/TechnicalData/submodel/submodelElements/FurtherInformation/Test")))
        // Agent backendUrl + agent backendAuth
        executeHandler("urn:scsn:12345678", "/1.0.2/orderstatus")
        wiremock.verify(exactly(1), getRequestedFor(urlEqualTo("/scsn12345678/1.0.2/orderstatus")))
        // Agent backendUrl + agent backendAuth
        executeHandler("urn:scsn:12345678", "/0.4.6/orderstatus")
        wiremock.verify(exactly(1), getRequestedFor(urlEqualTo("/scsn12345678/0.4.6/orderstatus")))
        // root backendBaseUrlMapping + agent backendAuth
        executeHandler("urn:scsn:23456789", "/0.4.6/orderstatus")
        wiremock.verify(exactly(1), getRequestedFor(urlEqualTo("/v046/orderstatus")))
        // root backendBaseUrlMapping + agent backendAuth
        executeHandler("urn:scsn:23456789", "/1.0.2/orderstatus")
        wiremock.verify(exactly(1), getRequestedFor(urlEqualTo("/v102/orderstatus")))
        // agent backendUrlMapping + root backendAuth
    }

    @Test
    fun testServer() {
        HttpClients.createDefault().execute(
            HttpGet("http://localhost:$port/openapi/urn:aas:1234/urn:aas:1234/v1/shells/www.tata.com.1687591.01.01/aas/submodels/TechnicalData/submodel/submodelElements/FurtherInformation/Test?queryParam=test").apply {
                addHeader("Forward-To", "urn:ids:connector")
                addHeader("Forward-Recipient", "urn:aas:1234")
                addHeader("Forward-AccessUrl", "http://localhost:47376")
            }
        ).use {
            Assertions.assertEquals(200, it.statusLine.statusCode)
            Assertions.assertEquals("Test-With-Mask:http://localhost:$port/openapi/urn:aas:1234/urn:aas:1234/v1/Things", it.entity.content.readAllBytes().decodeToString())
            Assertions.assertEquals("text/css", it.getFirstHeader("Content-Type").value)
        }
        HttpClients.createDefault().execute(
            HttpGet("http://127.0.0.1:$port/openapi/urn:aas:1234/urn:aas:1234/v1/shells?queryParam=test").apply {
                addHeader("Forward-To", "urn:ids:connector")
                addHeader("Forward-Recipient", "urn:aas:1234")
                addHeader("Forward-AccessUrl", "http://localhost:47376")
            }
        ).use {
            Assertions.assertEquals(200, it.statusLine.statusCode)
            Assertions.assertEquals("Test-With-Mask:http://127.0.0.1:$port/openapi/urn:aas:1234/urn:aas:1234/v1/Things", it.entity.content.readAllBytes().decodeToString())
            Assertions.assertEquals("text/css", it.getFirstHeader("Content-Type").value)
        }
        println()
    }

    private fun executeHandler(agent: String, apiPath: String) {
        val multiPartMessage = MultiPartMessage(
            InvokeOperationMessageBuilder()
                ._operationReference_(URI.create("https://ids.tno.nl/openapi$apiPath"))
                ._issuerConnector_(URI.create("urn:ids:connector"))
                ._senderAgent_(URI.create(agent))
                ._recipientConnector_(Util.asList(URI.create("urn:ids:connector")))
                ._recipientAgent_(Util.asList(URI.create(agent)))
                .build()
                .defaults(),
            "",
            httpHeaders = mutableMapOf(
                TYPE to "HTTP-Encoded",
                API_METHOD to "GET",
                API_PATH to apiPath
            )
        )
        val test = openApiMessageHandler.handle(multiPartMessage)
        Assertions.assertEquals(200, test.statusCode.value())
    }

    companion object {
        private lateinit var wiremock: WireMockServer

        @BeforeAll
        @JvmStatic
        fun setupWireMock() {
            val resultMessage = ResultMessageBuilder()
                ._issuerConnector_(URI.create("urn:ids:connector"))
                ._senderAgent_(URI.create("urn:aas:1234"))
                ._recipientConnector_(Util.asList(URI.create("urn:ids:connector")))
                ._recipientAgent_(Util.asList(URI.create("urn:aas:1234")))
                ._correlationMessage_(URI("urn:ids:message:1"))
                .build()
                .defaults()
                .toMultiPartMessage(
                    payload = "Test-With-Mask:$BACKEND_URL_MASK/Things",
                )
            wiremock = WireMockServer(47376).apply { start() }
            wiremock.stubFor(
                any(urlMatching("/cc/api.*")).willReturn(ok())
            )
            wiremock.stubFor(
                any(urlMatching("/cc/forward.*"))
                    .willReturn(ok()
                        .withBody(resultMessage.toString())
                        .withHeader("Content-Type", resultMessage.httpHeaders["Content-Type"])
                        .withHeader(STATUS_CODE, "200")
                        .withHeader(TYPE, "HTTP-Encoded")
                        .withHeader(BACKEND_MASKED, "true")
                        .withHeader("${HEADER_PREFIX}content-type", "text/css")
                        .withHeader("${HEADER_PREFIX}Test", "Test")
                    )
            )
            wiremock.stubFor(
                any(urlMatching("/v046.*"))
                    .withHeader("X-API-Key", equalTo("123456"))
                    .willReturn(ok()
                        .withBody("""TestMasking-http://localhost:47376/v046/Test-Test-http://localhost:47376/v046-Test"""))
            )
            wiremock.stubFor(
                any(urlMatching("/v102.*"))
                    .withHeader("X-API-Key", equalTo("123456"))
                    .willReturn(ok()
                        .withBody("""TestMasking-http://localhost:47376/v102/Test-Test-http://localhost:47376/v102-Test"""))
            )
            wiremock.stubFor(
                any(urlMatching("/scsn12345678.*"))
                    .withHeader("Authorization", equalTo("Bearer API-KEY"))
                    .willReturn(ok()
                        .withBody("""TestMasking-http://localhost:47376/scsn12345678/0.4.6/Order-Test"""))
            )
            wiremock.stubFor(
                any(urlMatching("/aas1234.*"))
                    .withHeader("Authorization", equalTo("Basic dXNlcjpwYXNz"))
                    .willReturn(ok()
                        .withBody("""TestMasking-http://localhost:47376/aas1234/Test"""))
            )
        }

        @AfterAll
        @JvmStatic
        fun destroyWireMock() {
            wiremock.stop()
        }
    }
}