const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: 'https://pef-b.bic-df.interconnect.ids.smart-connected.nl/openapi',
  },
  publicPath: "./"
});
