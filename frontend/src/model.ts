export interface BackendAuthentication {
  type: string;
  username?: string;
  password?: string;
  token?: string;
  header?: {
    [name: string]: string
  }
}
export interface AgentConfig {
  id: string;
  title?: string;
  config?: any;
  backendUrl?: string;
  backendUrlMapping?: {
    [version: string]: string
  };
  versions?: Array<string>;
  openApiBaseUrl?: string;
  backendAuthentication?: BackendAuthentication
}

export interface OpenApiConfig {
  isConsumerOnly?: boolean;
  strictApi?: boolean;
  openApiBaseUrl?: string,
  backendBaseUrl?: string,
  backendBaseUrlMapping?: {
    [version: string]: string
  },
  backendAuthentication?: BackendAuthentication,
  mapping?: {
    [header: string]: string
  },
  versions?: Array<string>,
  agents?: Array<AgentConfig>,
  validate?: boolean,
  usePolicyEnforcement?: boolean,
  routeLocalAgentsViaIDS?: boolean,
  maskBackendUrl?: boolean,
  maskReplacement?: string,
  encodeInHttp?: boolean
}

export interface HTMLEvent extends Event {
  readonly target: HTMLElement | null;
}

export interface PolicyAgent {
  id: string;
  title?: string;
  openApiBaseUrl: string;
  versions: {
    [version: string]: Array<{method: string; path: string}>
  }
}