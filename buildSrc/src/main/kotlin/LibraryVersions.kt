object LibraryVersions {
  const val kotlin = "1.9.21"
  const val tnoIds = "4.8.0"
  const val spring = "2.7.18"
  const val springDeps = "1.1.4"
  const val jib = "3.4.0"
  const val swagger = "2.1.14"
  const val json = "20231013"
  const val mongo = "4.9.0"
  const val kotlinXSerialization = "1.5.0"
  const val node = "3.4.0"
  object Test {
    const val embedMongo = "4.12.0"
    const val wiremock = "2.34.0"
  }
}